#1
select maker, type from Product
order by maker;
#2
select model, ram, price, screen from Laptop
where price > 1000;
#3
select * from Printer
where color = "y"
order by price;
#4
select model, speed, hd, cd from PC
where cd = "12x" or cd ="24x"
order by price;
#6
select speed, price from PC
where speed >500 and price < 700
order by price;
#7
select model, type, price from Printer
where price <300 and type != "Matrix"
order by type;
#8
select hd, model, speed, price from PC
where price between 400 and 600
order by hd;
#9
select PC.model, maker, speed, hd from PC, Product
where hd = 10 or hd =20 and maker ="A"
order by speed;
#10
select model, speed, hd,screen from Laptop
where screen >= 12
order by price;
#11
select model, type, price from Printer
where price <300
order by type;
#12
select ram, price, model from Laptop
where ram =64
order by screen desc;
#13
select model, ram, price, hd from PC
where ram >64
order by hd desc;
#14
select model, speed , price from PC
where speed between 500 and 750
order by hd asc;
#2.1
select * from PC
where model rlike '1{2}';
#3.1
select maker, speed,type,hd from Product, PC
where hd < 8;
#3.2
select maker from Product, PC
where speed > 600;
#3.3
select maker from Product, Laptop
where speed < 500;
#3.4
select model, hd, ram from Laptop
where hd =hd and ram =ram
order by model desc;
#3.6
select PC.model , maker FROM Product ,PC
where price <600;
#3.7
select Printer.model, maker from Printer,Product
where price > 300;
#3.8
select PC.model,price,maker from PC,Product
UNION
select Laptop.model,price,maker from Product,Laptop;
#3.9 -//-
#3.10
select maker , Laptop.model, type,speed from Laptop, Product
where speed <600;
#Second_part of hometask
#4.1
select maker from Product
where type in("PC");
#4.2
select * from Product
where type = all(select type from PC);
#4.3 
select maker from Product
where type = any(select type from PC);
#4.4
select maker from PC, Product, Laptop
where type in ("PC","Laptop");
#4.5
select distinct maker from Product
where type = all(select type from PC,Laptop);
#4.6
select maker from Product
where type = any(select type from PC,Laptop);
#4.7
select maker from Product
where type in ("PC") and type = all(select type from PC);
#4.10
select PC.model from PC,Product
where maker in ("A");
#4.11
select maker from Product, PC
where Product.model != PC.model;
#4.12
select Laptop.model,Laptop.price from Laptop,PC 
where Laptop.price > PC.price;
#5.1
select maker from Product
where exists (select type from PC );
#5.2
select maker from Product
where exists (select speed from PC where speed >750);
#5.3
select maker from Product
where exists (select PC.speed, Laptop.speed from PC,Laptop where PC.speed > 750 and Laptop.speed > 750 );
#5.4
select maker from Product
where exists(select maker, max(PC.speed) from Product,PC where type = "Printer" );
#5.7
select maker from Product
where type in ("Laptop","Printer");
#5.8 
select maker from Product
where exists (select maker from Product where type = "Laptop" and type !="Printer");
#6.1
select "Averege price", avg(price)
from Laptop;
#6.2
select "модель : ", model,"ціна : ", price from PC;
#7.1
select model,max(price) from Printer;
#7.2
select distinct Laptop.model, Laptop.price, min(PC.price) as sp1d from Laptop,PC,Product
where  Laptop.speed < all(select PC.speed from PC);
#Task 8.1
select maker, sum(stats.pc) as pc ,sum(stats.laptop) as laptop,sum(stats.printer) as printer
from (select maker, 
		(select count(*) from pc where product.model = pc.model and product.maker = maker) as pc,
		(select count(*) from laptop where product.model = laptop.model) as laptop,
		(select count(*) from printer where product.model = printer.model) as printer 
	 from product) as stats
group by maker;
#Task 8.2
select maker , avg(laptops.screen) as size
from (select maker,
		(select avg(screen) from laptop where product.model = laptop.model and product.maker = maker) as screen
		from product) as laptops
group by maker having size is not null;
#Task 8.3
select maker, max(max_price) as max_price #max price for each maker
from (select maker,
		(select max(price) from pc where product.model = pc.model and product.maker = maker) as max_price #max price for each model
		from product) as pcs
group by maker having max_price is not null;
#Task 8.4  
select maker, min(min_price) as min_price #min price for each maker
from (select maker,
		(select min(price) from pc where product.model = pc.model and product.maker = maker) as min_price  #min price for each model
		from product) as pcs
group by maker having min_price is not null;

#Task 8.5
select speed, avg(price)
from pc
where speed > 600
group by speed;

#Homework tasks group 8 (Підзапити у якості обчислювальних стовпців)
#Task 8.6
select maker, avg(hd)
from (select product.maker, pc.*
		from pc, product
        where product.maker in (select distinct maker from product where type = 'printer') 
        and product.model = pc.model) as mks
group by maker;
#Task 9.1
select product.maker,
	case 
		when product.maker = pc_maker.maker then concat('yes(', pc_maker.amount,')')
        when product.maker not in (select distinct maker from product where type ='pc')   then 'no'
        else null
	end as make_pc
from (select product.maker, count(*) as amount  
            from product,pc 
            where pc.model = product.model
			group by product.maker) as pc_maker, product
group by product.maker, make_pc having make_pc is not null;    
#Task 10.1 
select product.maker, all_in_one.model, all_in_one.price
from (select pc.model , pc.price 
	from pc
	union all
	select laptop.model , laptop.price 
	from laptop
	union all
	select  printer.model , printer.price 
	from printer) as all_in_one, product
where product.maker = 'B' and product.model = all_in_one.model;
#Task 2
select product.type, all_in_one.model, max(all_in_one.price) as max_price
from (select pc.model , pc.price 
	from pc
	union all
	select laptop.model , laptop.price 
	from laptop
	union all
	select  printer.model , printer.price 
	from printer) as all_in_one, product
where product.model = all_in_one.model
group by model;
#Task 3. БД «Комп. фірма». Знайдіть середню ціну ПК та ноутбуків, що випущені виробником 'A'. Вивести: одна загальна середня ціна. (Підказка: використовувати оператор UNION)
select product.maker, avg(all_in_one.price) as avg_price_pc_and_laptop
from (select pc.model , pc.price 
	from pc
	union all
	select laptop.model , laptop.price 
	from laptop
	union all
	select  printer.model , printer.price 
	from printer) as all_in_one, product
where product.model = all_in_one.model and (product.type ='pc' or product.type ='laptop')
group by maker;










 
